﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCodeFistApplication
{
   public class Teacher
    {
        public int Id { get; set; }
        public string TeacherName { get; set; }
        public TeachtingMode ModeOfTeaching { get; set; }

    }
    public enum TeachtingMode
    {
        Online,
        ClassRoom,
        LiveOnline
    }

}
